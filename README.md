# README #

This is a simple shooting game written in Java. This was done as a homework in "Basic Programming Course in Java" in IT College.
Screenshot of the game:
![Game_screenshot.png](https://bitbucket.org/repo/zbLgeE/images/377066058-Game_screenshot.png)


Pictures used in this game are taken from :
http://neopets.wikia.com/wiki/Special:NewFiles?file=Dimensional_Eyrie.png
http://stuffpoint.com/neopets/image/257701/lost-desert-wallpaper/
Copyright disclaimer: I do NOT own these images featured in the game. All rights belong to it's rightful owner/owner's. No copyright infringement intended.
These pictures are used for educational purposes only!

For further information contact Laur Telliskivi ltellisk@itcollege.ee