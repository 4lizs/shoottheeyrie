package elements;

import static org.junit.Assert.*;

import org.junit.Test;

public class TargetTest {

	@Test
	public void testMove1() {
		// Here we test if object coordinates change correctly
		// when move() method is called.
		Target target = new Target();
		target.setX(200);
		target.setY(300);
		target.setxSpeed(-2);
		target.setySpeed(4);
		int y = target.getY() + target.getySpeed();
		int x = target.getX() + target.getxSpeed();
		// run the tested method
		target.move();
		assertEquals(y, target.getY());
		assertEquals(x, target.getX());
	}
	@Test
	public void testIsFlipped (){
		Target target = new Target();
		//Set x out of gamepanel bounds so target must flip itself
		target.setX(1500);
		target.setY(300);
		target.setxSpeed(2);
		target.setySpeed(3);
		target.move();
		assertTrue(target.isFlipped());
		assertEquals(-2, target.getxSpeed());
	}

}
