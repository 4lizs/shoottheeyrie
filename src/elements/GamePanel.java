package elements;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.Timer;
/**
 * This panel implements a simple shooting game where user tries to
 * eliminate as much targets (Eyries) as possible in given time. Targets are
 * created and move around the panel randomly. When time is up, the result is
 * displayed.
 * @author Laur
 *
 */
@SuppressWarnings("serial")
public class GamePanel extends JPanel {
	/**
	 * HitCircle animation
	 */
	private HitCircle hit;
	/**
	 * Frame count of the timer
	 */
	private int frameNr = 0;
	/**
	 * Needed to activate focusGained only once
	 */
	private int focusCount = 0;
	/**
	 * Game time
	 */
	private int startTime = 10;
	/**
	 * Shows seconds left until game ends
	 */
	private int secondsLeft = startTime;
	/**
	 * Needed to count the second
	 */
	private int n = 1;
	/**
	 * Gamepanel width
	 */
	public static int rangeWidth = 1400;
	/**
	 * Gamepanel height
	 */
	public static int rangeHeight = 900;
	/**
	 * Array list of targets
	 */
	private List<Target> targets = new ArrayList<Target>();
	/**
	 * User controlled crosshair
	 */
	private Crosshair crosshair = new Crosshair();
	/**
	 * Timer that drives the game
	 */
	final Timer timer;
	/**
	 * Shows number of targets eliminated
	 */
	private int targetsHit = 0;
	/**
	 * Number of targets in ArrayList
	 */
	private int numberOfTargets = 4;
	/**
	 * Status of the game. Is it running or not?
	 */
	private boolean running = true;
	/**
	 * Coordinates of the crosshair when target shot
	 */
	private Point chPoint;
	/**
	 * Background image
	 */
	private BufferedImage bgImage;
	/**
	 * Constructs the GamePanel
	 */
	public GamePanel() {
		// Initialize background picture
		setBackground();
		// set the preffered size of the GamePanel
		this.setPreferredSize(new Dimension(rangeWidth, rangeHeight));
		
		// Create timer that drives the game and listener of type ActionListener
		timer = new Timer(10, new ActionListener() {
			//code in this method will be executed every time the timer generates an event, in our case every 10ms
			public void actionPerformed(ActionEvent evt) {
				frameNr++;//count frame numbers
				if (frameNr / n == 100) {// this is needed to count the seconds
					n++;
					secondsLeft--;
					if (secondsLeft == 0) {//if time is up, end the game
						gameOver();
					}
				}
				updateGame();
			}
		});
		//Listen mouse events from the GamePanel
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				// The mouse listener requests focus when user
				// clicks the panel. 
				requestFocus();
				//Get the x,y coordinate of the mouse click
				chPoint = e.getPoint();
				//Call the method that checks if target was hit
				validateHit();
			}
		});
		
		//Listen focus events from the GamePanel
		addFocusListener(new FocusAdapter() {
			public void focusGained(FocusEvent evt) {
				if (focusCount == 0){
					timer.start();
					focusCount++;
				}
			}
		});
		
		// Listen mouse motion events from the GamePanel
		addMouseMotionListener(new MouseAdapter() {
			public void mouseMoved(MouseEvent e) {
				//Update the crosshair coordinates when mouse is moved
				moveCrosshair(e.getX(), e.getY());
			}
		});
		
		// Listen key events from the GamePanel
		addKeyListener(new KeyAdapter() {
			public void keyPressed (KeyEvent e){
				if(running == false){
					int key = e.getKeyCode(); //key that is pressed
					switch(key){
					case KeyEvent.VK_SPACE:
						reset();
						timer.restart();
						running = true;
						break;
					case KeyEvent.VK_ESCAPE:
						System.exit(0); //exit game and screen
					}
				}
			}
		});

	}
	/**
	 * Reset game parameters for a new game
	 */
	public void reset(){
		hit = null;
		frameNr = 0;
		n = 1;
		targetsHit = 0;
		secondsLeft = startTime;
	}
	/**
	 * This method stops the game when its time
	 */
	public void gameOver() {
		timer.stop();
		running = false;
		targets.clear();//remove all the targets from the game
	}
	/**
	 * Adds new targets to the game if needed. Always keeps specified amounts of targets in the game while
	 * time is running. Also updates the targets position in the game.
	 */
	public void updateGame() {
		if (secondsLeft != 0) {
			if (targets.size() < numberOfTargets) {
				targets.add(new Target());//add a new target to the game
			}
			for (int i = 0; i < targets.size(); i++) {
				targets.get(i).move();
			}
		}
	}

	/**
	 * This method checks if the target is hit.
	 * If the target has been hit, it will remove the target from
	 * the arraylist and creates hit animation.
	 */
	public void validateHit() {
			//Reverse loop is needed if we want to eliminate the target who is on top
			//in case 2 or more targets happen to be at the same area where the hit was made.
			for (int i = targets.size() - 1; i >= 0; i--) {
				// we create target image size rectangle to detect if image was hit
				if (new Rectangle(targets.get(i).getX(), targets.get(i).getY(), targets.get(i).getWidth(),
						targets.get(i).getHeight()).contains(chPoint)) {
					targets.remove(i);//remove from game
					hit = new HitCircle(chPoint.x, chPoint.y, 1 );//new hit animation object
					targetsHit++;//count the hits
					break;
				}
			}

//		}

	}	
	/**
	 * This method updates the crosshair position
	 * @param x
	 * @param y
	 */
	public void moveCrosshair(int x, int y) {
		/**
		 * Current crosshair state
		 */
		final int current_X = crosshair.getX();
		final int current_Y = crosshair.getY();

		if ((current_X != x) || (current_Y != y)) { // crosshair is moving
		
			//Set new coorinates to the crosshair
			crosshair.setX(x);
			crosshair.setY(y);
			
		}
	}
	/**
	 * Draws the game to the sceen.
	 */
	@Override
	public void paintComponent(Graphics g) {
		g.drawImage(bgImage, 0, 0, null);
		if (hasFocus()) {
			setBorder(BorderFactory.createLineBorder(Color.CYAN, 5));
		} else {
			setBorder(BorderFactory.createLineBorder(Color.GRAY, 5));
			g.setFont(new Font(null, Font.PLAIN, 30));
			g.setColor(Color.getHSBColor((float)Math.random(), 
					(float)Math.random(), (float) Math.random()));//having a bit fun :)
			g.drawString("CLICK TO START GAME", (rangeWidth / 2) - 100, rangeHeight / 2);
			g.setColor(new Color(211, 211, 211, 255));
			g.drawString("Eliminate as many Eyries you can in " + startTime + " seconds!", 
					(rangeWidth / 2) - 200, (rangeHeight /2) + 30);
		}
		// This loop draws all our moving targets
		for (int i = 0; i < targets.size(); i++) {
			targets.get(i).drawImage(g);
		}
		if(hit != null){ // If HitCircle object created, draw circle
			hit.drawHit(g);
			hit.setRadius(hit.getRadius() + 1);//increment the radius ppf
			if (hit.getRadius() == 50){ //if radius reaches 50, remove circle
				hit = null;
			}
		}
		// draws crosshair
		crosshair.drawItself(g);
		//Draw game texts when game is running
		g.setFont(new Font(null, Font.PLAIN, 20));
		g.drawString("Laur Telliskivi , r�hm IA17", 20, 30);
		g.drawString("Seconds left : " + secondsLeft, 900, 30);
		g.drawString("Targets hit: " + targetsHit, 600, 30);
		g.drawString("Copyright disclaimer: I do NOT own these images featured in the game. "
				+ "All rights belong to it's rightful owner/owner's. "
				+ "No copyright infringement intended. ", 20, rangeHeight - 30);
		g.drawString("These pictures are used for noncommercial purposes only!", 20, rangeHeight - 10);
		if (running == false) {
			//Game Over texts
			g.setColor(Color.GREEN);
			g.drawString("WELL DONE ! YOU ELIMINATED " + targetsHit + " EYRIES!", (int) (rangeWidth * 0.4),
					(int) (rangeHeight / 2));
			g.drawString("PRESS SPACE BAR TO TRY AGAIN OR ESC TO QUIT", (int) (rangeWidth * 0.4), (int) (rangeHeight / 2) + 30);

		}
	}
	/**
	 * Loads the background image
	 */
	public void setBackground() {
		try {
			bgImage = ImageIO.read(getClass().getResource("/Desert.jpg"));
		} catch (IOException e) {
			System.out.println("Could not find picture 2!");
			System.exit(0);
		}

	}


}
