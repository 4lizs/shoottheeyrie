package elements;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

/**
 * Crosshair class
 * @author Laur
 *
 */
public class Crosshair{
	/**
	 * Crosshair radius
	 */
	private static final int radius = 20;
	/**
	 * X coordinate of the crosshair.
	 */
	private int xPos;
	/**
	 * Y coordinate of the crosshair. 
	 */
	private int yPos;
	/**
	 * Height of the crosshair
	 */
	private static final int height = 40;
	/**
	 * Width of the crosshair
	 */
	private static final int width = 40;
	/**
	 * Set new x coordinate for crosshair
	 * @param xPos
	 */
	public void setX(int xPos) {
		this.xPos = xPos;
	}
	/**
	 * Getter method for xPos
	 * @return xPos
	 */
	public int getX (){
		return xPos;
	}
	/**
	 * set new y coordinate for crosshair
	 * @param yPos
	 */
	public void setY(int yPos) {
		this.yPos = yPos;
	}
	/**
	 * Getter method for yPos
	 * @return yPos
	 */
	public int getY (){
		return yPos;
	}
	/**
	 * Getter method for crosshair height
	 * @return height
	 */
	public int getHeight (){
		return height;
	}
	/**
	 * Getter method for crosshair width
	 * @return width
	 */
	public int getWidth (){
		return width;
	}
	/**
	 * Draw crosshair
	 */
	public void drawItself(Graphics g) {	
		// extending graphics context with Graphics2D class
		Graphics2D g2 = (Graphics2D) g;
		//Setting the crosshair line thickness
		g2.setStroke(new BasicStroke(3));
		g.setColor(Color.GREEN);
		g.drawLine(xPos - radius, yPos, xPos + radius, yPos);
		g.drawLine(xPos, yPos - radius, xPos, yPos + radius);
		g.drawOval(xPos - radius, yPos - radius, height, width);

	}

}
