package elements;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;

/**
 * Target class
 * 
 * @author Laur
 *
 */
public class Target {
	/**
	 * Random number generator
	 */
	private Random random = new Random();
	/**
	 * X coordinate of the target.
	 */
	private int x;
	/**
	 * Y coordinate of the target.
	 */
	private int y;
	/**
	 * Target speed along the x coordinate
	 */
	private int xSpeed;
	/**
	 * Target speed alond the y coordinate
	 */
	private int ySpeed;
	/**
	 * Is the target flipped?
	 */
	private boolean isFlipped = false;
	/**
	 * Target image
	 */
	private BufferedImage targetImg;

	/**
	 * Create a new Target object. It has a starting x, y coordinates and speed
	 */
	public Target() {
		try {
			targetImg = ImageIO.read(getClass().getResource("/Dimensional_Eyrie.png"));
		} catch (IOException e) {
			System.out.println("Could not find the picture!");
			System.exit(0);
		}
		this.x = random.nextInt(GamePanel.rangeWidth - getWidth());
		this.y = random.nextInt(GamePanel.rangeHeight - getHeight());
		this.xSpeed = random.nextInt(5) + 1;// make 0 exclusive
		this.ySpeed = random.nextInt(5) + 1;// make 0 exclusive
	}
	/**
	 * This method moves the target around the coordinate space of the panel
	 */
	public void move() {

		x += xSpeed;
		y += ySpeed;
		// If target reaches the end of right or left screen,
		// it will start moving the opposite direction and flips the image
		if (x + getWidth() > GamePanel.rangeWidth) {
			xSpeed = xSpeed * (-1);
			x += xSpeed;
			isFlipped = true;
		}
		if (x < 0) {
			xSpeed = xSpeed * (-1);
			x += xSpeed;
			isFlipped = false;
		}
		// If target reaches the bottom or upper part of the screen
		// it will start moving the opposite direction
		if (y + getHeight() > GamePanel.rangeHeight || y < 0) {
			ySpeed = ySpeed * (-1);
			y += ySpeed;
		}

	}

	/**
	 * Method for drawing target image to the screen
	 */
	public void drawImage(Graphics g) {
		if (isFlipped()) {
			g.drawImage(targetImg, x + getWidth(), y, -getWidth(), getHeight(), null);
		} else
			g.drawImage(targetImg, x, y, null);
	}

	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}

	public int getHeight() {
		return targetImg.getHeight();
	}

	public int getWidth() {
		return targetImg.getWidth();
	}

	public int getxSpeed() {
		return xSpeed;
	}

	public void setxSpeed(int xSpeed) {
		this.xSpeed = xSpeed;
	}

	public int getySpeed() {
		return ySpeed;
	}

	public void setySpeed(int ySpeed) {
		this.ySpeed = ySpeed;
	}

	public boolean isFlipped() {
		return isFlipped;
	}

	public void setFlipped(boolean isFlipped) {
		this.isFlipped = isFlipped;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

}
