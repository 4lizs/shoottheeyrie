package elements;

import java.awt.Color;
import java.awt.Graphics;
/**
 * Simple class that holds the size, location and color
 * of the colored circle. It has a method for drawing
 * the circle in graphics context which draw the circle as oval.
 * @author Laur
 *
 */
public class HitCircle {
	/**
	 * Hitpoint x coordinate
	 */
	private int hpX;
	/**
	 * Hitpoint y coordinate
	 */
	private int hpY;
	/**
	 * Circle radius
	 */
	private int radius;
	/**
	 * Circle color
	 */
	private Color hpColor;
	
	/**
	 * Create HitCircle with a given location, radius.
	 * The circle will be red colored.
	 * @param x
	 * @param y
	 * @param rad
	 */
	public HitCircle(int x, int y, int rad){
		this.hpX = x;
		this.hpY = y;
		this.radius = rad;
		final int red = 230;
		hpColor = new Color(red, 0, 0, 150);
	}
	/**
	 * Draw the circle
	 * @param g
	 */
	public void drawHit(Graphics g){
		g.setColor(hpColor); // fill circle with red color
		g.fillOval(hpX - radius, hpY - radius, 2 * radius, 2 * radius);
		g.setColor(Color.WHITE);// paint outline of the circle white
		g.drawOval(hpX - radius, hpY - radius, 2 * radius, 2 * radius);
	}
	/**
	 * @return the radius
	 */
	public int getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(int radius) {
		this.radius = radius;
	}
}
