package shoot;

import java.awt.Point;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import elements.GamePanel;

/**
 * This class contains main() routine that allows the game to be run
 * as program
 * @author Laur
 *
 */
public class StartGame {
	
	public static void main(String[] args) {
		//Using here the SwingUtilities helper class to construct the UI in 
		//Event Dispatch Thread
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				createGameGUI();
				
			}
		});
	}
	private static void createGameGUI(){
		
		JFrame screen = new JFrame ("Shoot the Eyrie!");
		screen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		// Make default cursor transparent
		screen.setCursor(screen.getToolkit().createCustomCursor(
	            new BufferedImage(3, 3, BufferedImage.TYPE_INT_ARGB), new Point(0, 0),
	            "blank"));
		// Adding GamePanel to the parent container JFrame
		screen.add(new GamePanel());
		//Change the frame to its preferred size
		screen.pack();
		// Set screen not resizable
		screen.setResizable(false);
		//Set screen to the center
		screen.setLocationRelativeTo(null);
		screen.setVisible(true);
		
	}

}
